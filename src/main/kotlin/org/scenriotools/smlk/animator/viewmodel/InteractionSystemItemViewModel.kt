package org.scenriotools.smlk.animator.viewmodel

import org.scenriotools.smlk.animator.model.InteractionSystem
import tornadofx.*

class InteractionSystemItemViewModel(interactionSystem: InteractionSystem) : ItemViewModel<InteractionSystem>(interactionSystem){
    val name = bind(InteractionSystem::name)
    val width = bind(InteractionSystem::width)
    val height = bind(InteractionSystem::height)
    val currentEvents = bind(InteractionSystem::currentEvents)
    val instances = bind(InteractionSystem::instances)
    val canMakeNextStep = bind(InteractionSystem::canMakeNextStepProperty)
    val scenarioProgramIsRunning = bind(InteractionSystem::scenarioProgramIsRunningProperty)
    val selectedConfigurationName = bind(InteractionSystem::selectedConfigurationNameProperty)
    val configurationNames = bind(InteractionSystem::configurationNames)

    fun makeStep() = item.makeStep()
    fun play() = item.play()
    fun exportSequenceDiagram() = item.exportSequenceDiagram()

}
