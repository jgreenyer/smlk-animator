package org.scenriotools.smlk.animator.viewmodel

import org.scenriotools.smlk.animator.model.Instance
import org.scenriotools.smlk.animator.model.InteractionSystem
import tornadofx.ItemViewModel

class InstanceViewModel(instance: Instance) : ItemViewModel<Instance>(instance){
    val name = bind(Instance::name)
    val dependencies = bind(Instance::dependencies)
}