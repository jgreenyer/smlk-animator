package org.scenriotools.smlk.animator.view

import javafx.beans.InvalidationListener
import javafx.beans.Observable
import javafx.beans.property.DoubleProperty
import javafx.geometry.Point2D
import javafx.scene.Group
import javafx.scene.shape.Line
import org.scenriotools.smlk.animator.style.InteractionAnimatorStyles
import tornadofx.CssRule
import tornadofx.addClass
import tornadofx.style


class Arrow private constructor(line: Line, arrow1: Line, arrow2: Line, cssRule : CssRule) : Group(line, arrow1, arrow2) {
    private val line: Line

    constructor() : this(Line(), Line(), Line(), InteractionAnimatorStyles.interactionArrow)
    constructor(start : Point2D, end : Point2D, cssRule : CssRule) : this(start.x, start.y, end.x, end.y, cssRule)
    constructor(startX: Double, startY: Double, endX: Double, endY: Double, cssRule : CssRule) : this(Line(), Line(), Line(), cssRule) {
        this.startX = startX
        this.startY = startY
        this.endX = endX
        this.endY = endY
    }

    // start/end properties
    var startX: Double
        get() = line.startX
        set(value) {
            line.startX = value
        }

    fun startXProperty(): DoubleProperty {
        return line.startXProperty()
    }

    var startY: Double
        get() = line.startY
        set(value) {
            line.startY = value
        }

    fun startYProperty(): DoubleProperty {
        return line.startYProperty()
    }

    var endX: Double
        get() = line.endX
        set(value) {
            line.endX = value
        }

    fun endXProperty(): DoubleProperty {
        return line.endXProperty()
    }

    var endY: Double
        get() = line.endY
        set(value) {
            line.endY = value
        }

    fun endYProperty(): DoubleProperty {
        return line.endYProperty()
    }

    fun middleX() = middle(startX, endX)
    fun middleY() = middle(startY, endY)

    companion object {
        private const val arrowLength = 20.0
        private const val arrowWidth = 7.0
    }

    init {
        this.line = line
        line.addClass(cssRule)
        arrow1.addClass(cssRule)
        arrow2.addClass(cssRule)
        val updater = InvalidationListener { _: Observable? ->
            val ex = endX
            val ey = endY
            val sx = startX
            val sy = startY
            arrow1.endX = ex
            arrow1.endY = ey
            arrow2.endX = ex
            arrow2.endY = ey
            if (ex == sx && ey == sy) { // arrow parts of length 0
                arrow1.startX = ex
                arrow1.startY = ey
                arrow2.startX = ex
                arrow2.startY = ey
            } else {
                val factor = arrowLength / Math.hypot(sx - ex, sy - ey)
                val factorO = arrowWidth / Math.hypot(sx - ex, sy - ey)
                // part in direction of main line
                val dx = (sx - ex) * factor
                val dy = (sy - ey) * factor
                // part ortogonal to main line
                val ox = (sx - ex) * factorO
                val oy = (sy - ey) * factorO
                arrow1.startX = ex + dx - oy
                arrow1.startY = ey + dy + ox
                arrow2.startX = ex + dx + oy
                arrow2.startY = ey + dy - ox
            }
        }
        // add updater to properties
        startXProperty().addListener(updater)
        startYProperty().addListener(updater)
        endXProperty().addListener(updater)
        endYProperty().addListener(updater)
        updater.invalidated(null)
    }
}

fun middle(x1:Double, x2:Double):Double{
    val delta =x2 - x1
    return x1 + (delta / 2)
}