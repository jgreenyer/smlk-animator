package org.scenriotools.smlk.animator.view

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Group
import javafx.scene.control.Alert
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.ButtonType
import javafx.scene.effect.DropShadow
import javafx.scene.layout.AnchorPane
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import org.scenriotools.smlk.animator.model.*
import org.scenriotools.smlk.animator.style.InteractionAnimatorStyles
import org.scenriotools.smlk.animator.viewmodel.InstanceViewModel
import org.scenriotools.smlk.animator.viewmodel.InteractionSystemItemViewModel
import tornadofx.*


class InteractionAnimatorView(): View("Interaction Animator") {


   val isvm = InteractionSystemItemViewModel(
      InteractionSystem(
      "temp",
     1200.0,
     800.0,
      scenarioProgram = null
      )
   )


   // every view has a root component
   override val root = borderpane{
      title = "Interaction Animator"
      top{
         hbox{
            combobox<String> {
               disableWhen { isvm.scenarioProgramIsRunning}
               items = isvm.configurationNames.value
               valueProperty().bindBidirectional(isvm.item.selectedConfigurationNameProperty)
               selectionModel.selectFirst()
            }
            button {
               enableWhen { isvm.canMakeNextStep}
               text = "Make Step"
               action{
                  val result = isvm.makeStep()
                  if (!result.equals("")){
                     val alert = Alert(
                        AlertType.ERROR,
                        result,
                        ButtonType.OK
                     )
                     alert.showAndWait()
                  }
               }
            }
            button {
               //disableWhen { isvm.canMakeNextStep }
               text = "Export To Sequence Diagram"
               action{
                  isvm.exportSequenceDiagram()
               }
            }
//            button {
//               enableWhen(isvm.canMakeNextStep)
//               text = "Play"
//               action{
//                  val result = isvm.play()
//                  if (!result.equals("")){
//                     val alert = Alert(
//                        AlertType.ERROR,
//                        result,
//                        ButtonType.OK
//                     )
//                     alert.showAndWait()
//                  }
//               }
//            }
         }
      }
      center{
         stackpane {
            alignment = Pos.TOP_LEFT
            group{
               rectangle {
                  addClass(InteractionAnimatorStyles.canvas)
                  useMaxHeight = true
                  useMaxWidth = true
                  widthProperty().bindBidirectional(isvm.item.widthProperty)
                  heightProperty().bindBidirectional(isvm.item.heightProperty)
//                  width = isvm.width.value
//                  height = isvm.height.value
               }
               group{
                  children.bind(isvm.instances.value){
                     anchorpane{
                        val instanceViewModel = InstanceViewModel(it)

                        children.bind(instanceViewModel.dependencies.value){ target ->
                           anchorpane {
                              add(createArrow(it, target, InteractionAnimatorStyles.dependencyArrow))
                           }
                        }

                        stackpane {
                           AnchorPane.setLeftAnchor(this, it.xPos)
                           AnchorPane.setTopAnchor(this, it.yPos)
                           rectangle {
                              addClass(InteractionAnimatorStyles.instance)
                              x = it.xPos
                              y = it.yPos
                              fill = it.bgcolor
                              stroke = it.color
                              width = it.width
                              height = it.height
                              val dropShadow = DropShadow()
                              dropShadow.radiusProperty().bind(it.shadowSizeProperty)
                              dropShadow.spread = 0.5
                              dropShadow.color = Color.RED
                              effect = dropShadow
                           }
                           vbox{
                              hbox{
                                 alignment = Pos.CENTER
                                 label(it.name){
                                    isWrapText = true
                                    maxWidth = it.width - 10
                                    padding = Insets(2.0, 2.0, 0.0, 2.0)
                                    textFill = it.color
                                    addClass(InteractionAnimatorStyles.instanceLabel)
                                 }
                              }
                              for(p in it.getPropertyList()){
                                 hbox{
                                    alignment = Pos.BASELINE_LEFT
                                    label(" ${p.first} = "){
                                       textFill = it.color
                                       addClass(InteractionAnimatorStyles.instancePropertyLabel)
                                    }
                                    val propLabel = label("${p.second}"){
                                       maxWidth = it.width - 10
                                       //padding = Insets(0.0, 2.0, 0.0, 2.0)
                                       textFill = it.color
                                       addClass(InteractionAnimatorStyles.instancePropertyLabel)
                                    }
                                    propLabel.textProperty().bind(p.second)
                                 }
                              }
                           }
                        }
                     }
                  }
               }

               group{
                  children.bind(isvm.currentEvents.value){
                     group {
                        when (it) {
                           is InstanceEvent -> {
                              val arrow = createArrow(it, InteractionAnimatorStyles.interactionArrow)
                              add(arrow)
                              anchorpane {
                                 stackpane {
                                    AnchorPane.setLeftAnchor(this, arrow.middleX())
                                    AnchorPane.setTopAnchor(this, arrow.middleY())
                                    val label = label(it.name) {
                                       addClass(InteractionAnimatorStyles.interactionLabel)
                                    }
                                    add(label)
                                 }
                              }
                           }
                           is LabelEvent -> {
                              createLabel(10.0, 10.0, 500.0, 100.0, it.label)
                           }
                           is InstanceLabelEvent -> {
                              createLabelRectangle(it.instance.xPos, it.instance.yPos, it.instance.width, it.instance.height, it.label)
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }
}

fun createArrow(instanceEvent : InstanceEvent, cssRule : CssRule) = when (instanceEvent){
   is InteractionEvent -> createArrow(instanceEvent.sourceInstance, instanceEvent.instance, cssRule)
   else -> createSelfArrow(instanceEvent.instance, cssRule)
}

fun createArrow(sourceInstance : Instance, targetInstance : Instance, cssRule : CssRule) : Arrow {
   if (sourceInstance == targetInstance)
      return createSelfArrow(targetInstance, cssRule)
   else {
      val startPoint = sourceInstance.intersectionPoint(targetInstance.middleX(), targetInstance.middleY())
      val endPoint = targetInstance.intersectionPoint(sourceInstance.middleX(), sourceInstance.middleY())
      return Arrow(startPoint, endPoint, cssRule)
   }
}

fun createSelfArrow(targetInstance : Instance, cssRule : CssRule) : Arrow{
   val originX = targetInstance.middleX() + targetInstance.width
   val originY = targetInstance.middleY() - targetInstance.height
   val startPoint = javafx.geometry.Point2D(originX, originY)
   val endPoint = targetInstance.intersectionPoint(originX, originY)
   return Arrow(startPoint, endPoint, cssRule)
}

fun Group.createLabelRectangle(xPos : Double, yPos:Double, width : Double, height : Double, labelText : String) =
   group{
      println("Label xPos=$xPos yPos=$yPos")
      rectangle {
         addClass(InteractionAnimatorStyles.instance)
         arcWidth = 25.0
         arcHeight = 25.0
         x = xPos
         y = yPos
         fill = Color.LIGHTYELLOW
         stroke = Color.BLACK
         this.width = width
         this.height = height
      }
      anchorpane{
         stackpane {
            AnchorPane.setLeftAnchor(this, xPos)
            AnchorPane.setTopAnchor(this, yPos)
            vbox {
               hbox {
                  alignment = Pos.CENTER
                  label(labelText) {
                     isWrapText = true
                     maxWidth = width - 10
                     padding = Insets(10.0, 10.0, 10.0, 10.0)
                     textFill = Color.BLACK
                     addClass(InteractionAnimatorStyles.instanceLabel)
                  }
               }
            }
         }
      }
   }

fun Group.createLabel(xPos : Double, yPos:Double, width : Double, height : Double, labelText : String) =
   group{
      anchorpane{
         stackpane {
            AnchorPane.setLeftAnchor(this, xPos)
            AnchorPane.setTopAnchor(this, yPos)
            vbox {
               hbox {
                  alignment = Pos.CENTER
                  label(labelText) {
                     font = Font.font("arial", FontWeight.BOLD, 20.0)
                     isWrapText = true
                     maxWidth = width - 10
                     maxHeight = height - 10
                     padding = Insets(10.0, 10.0, 10.0, 10.0)
                     textFill = Color.BLACK
                     //addClass(InteractionAnimatorStyles.instanceLabel)
                  }
               }
            }
         }
      }
   }
