package org.scenriotools.smlk.animator.view

import javafx.geometry.Point2D
import org.scenriotools.smlk.animator.model.Instance
import java.lang.IllegalArgumentException

fun Instance.middleX() = xPos + width / 2
fun Instance.middleY() = yPos + height / 2

/**
 * Finds the intersection point between
 *     * the instance rectangle
 *       with parallel sides to the x and y axes
 *     * the half-line pointing towards (x,y)
 *       originating from the middle of the rectangle
 *
 * Note: the function works given min(XY) <= max(XY),
 *       even though minY may not be the "top" of the rectangle
 *       because the coordinate system is flipped.
 * Note: if the input is inside the rectangle,
 *       the line segment wouldn't have an intersection with the rectangle,
 *       but the projected half-line does.
 * Warning: passing in the middle of the rectangle will return the midpoint itself
 *          there are infinitely many half-lines projected in all directions,
 *          so let's just shortcut to midpoint (GIGO).
 *
 * @return an object with x and y members for the intersection
 * @throws IllegalArgumentException if validate == true and (x,y) is inside the rectangle
 */
fun Instance.intersectionPoint(x : Double, y:Double) : Point2D {
    val minX = this.xPos
    val minY = this.yPos
    val maxX = this.xPos + width
    val maxY = this.yPos + height
    if ((minX < x && x < maxX) && (minY < y && y < maxY))
        throw IllegalArgumentException ("Point [$x,$y] cannot be inside the rectangle: [$minX, $minY] [$maxX, $maxY]")
    val midX = (minX + maxX) / 2
    val midY = (minY + maxY) / 2
    // if (midX - x == 0) -> m == ±Inf -> minYx/maxYx == x (because value / ±Inf = ±0)
    val m = (midY - y) / (midX - x)

    if (x <= midX) { // check "left" side
        val minXy = m * (minX - x) + y
        if (minY <= minXy && minXy <= maxY)
            return Point2D(minX,minXy)
    }

    if (x >= midX) { // check "right" side
        val maxXy = m * (maxX - x) + y
        if (minY <= maxXy && maxXy <= maxY)
            return Point2D(maxX, maxXy)
    }

    if (y <= midY) { // check "top" side
        val minYx = (minY - y) / m + x
        if (minX <= minYx && minYx <= maxX)
            return Point2D(minYx, minY)
    }

    if (y >= midY) { // check "bottom" side
        val maxYx = (maxY - y) / m + x
        if (minX <= maxYx && maxYx <= maxX)
//         return {x: maxYx, y: maxY};
            return Point2D(maxYx, maxY)
    }

    // edge case when finding midpoint intersection: m = 0/0 = NaN
    if (x.equals(midX) && y.equals(midY)) return Point2D(x,y)

    // Should never happen :) If it does, please tell me!
    throw IllegalArgumentException("Cannot find intersection for [$x,$y] inside rectangle [$minX, $minY] [$maxX, $maxY].")

}