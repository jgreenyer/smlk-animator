package org.scenriotools.smlk.animator.style

import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import tornadofx.*

class InteractionAnimatorStyles : Stylesheet(){
    companion object{
        val canvas by cssclass()
        val instance by cssclass()
        val instanceLabel by cssclass()
        val instancePropertyLabel by cssclass()
        val interactionArrow by cssclass()
        val interactionLabel by cssclass()
        val dependencyArrow by cssclass()
    }

    val canvasColor = c("#ffffff")
    val instanceBorderColor = c("#000000")
    val interactionArrowColor = c("#009999")
    val dependencyArrowColor = c("#333333")
    val interactionlabelColor = c("#005555")

    init {
        canvas{
            fill = canvasColor
        }
        instance{
            strokeWidth = 3.px
            stroke = instanceBorderColor
        }
        instanceLabel{
            font = Font.font("SanSerif", FontWeight.BOLD, 8.0)
        }
        instancePropertyLabel{
            font = Font.font("SanSerif", FontWeight.NORMAL, 8.0)
        }
        interactionArrow{
            stroke = interactionArrowColor
            strokeWidth = 3.px
        }
        dependencyArrow{
            stroke = dependencyArrowColor
            strokeDashArray = listOf(7.5.px, 7.5.px)
            strokeWidth = 1.px
        }
        interactionLabel{
            opacity = 0.9
            backgroundColor = multi(canvasColor)
            textFill = interactionlabelColor
            font = Font.font("SanSerif", FontWeight.NORMAL, 8.0)
        }
    }

}