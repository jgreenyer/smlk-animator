package org.scenriotools.smlk.animator.model

class InteractionEvent (val sourceInstance : Instance, targetInstance : Instance, name : String, vararg parameters : String) : InstanceEvent(targetInstance, name, *parameters){
}