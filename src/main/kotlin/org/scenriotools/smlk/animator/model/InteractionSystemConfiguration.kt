package org.scenriotools.smlk.animator.model

import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.smlkextension.LabelHelper
import kotlin.reflect.KFunction

class InteractionSystemConfiguration(val width : Double, val height : Double,
                                     val environmentMessageTypes : List<KFunction<ObjectEvent<*, *>>> = listOf(),
                                     val environmentObjects : Set<Any> = setOf(),
                                     val isEnvironmentEvent : (ObjectEvent<*, *>) -> Boolean = {
                                             objectEvent -> (objectEvent.sender?.let { it in environmentObjects } ?: false)
                                                || (environmentMessageTypes.containsKFunction(objectEvent.type))
                                                || (listOf(LabelHelper::envLabel, LabelHelper::envInstanceLabel).containsKFunction(objectEvent.type))},
                                     val eventsNotToVisualize : IEventSet = NOEVENTS,
                                     val instances : List<Instance>,
                                     val testScenario : suspend Scenario.() -> Unit,
                                     val assumptionScenarios : Set<suspend Scenario.() -> Unit> = setOf(),
                                     val guaranteeScenarios : Set<suspend Scenario.() -> Unit>){

    fun createScenarioProgram() : ScenarioProgram{
        val sp = ScenarioProgram(isEnvironmentEvent = isEnvironmentEvent)
        sp.environmentMessageTypes.addAll(environmentMessageTypes)
        sp.activeAssumptionScenarios.add(testScenario)
        sp.activeAssumptionScenarios.addAll(assumptionScenarios)
        sp.activeGuaranteeScenarios.addAll(guaranteeScenarios)
        return sp
    }

    fun test(){
        val sp = createScenarioProgram()
        sp.activeAssumptionScenarios.remove(testScenario)
        runTest(sp, timeoutMillis=2000, testScenario = testScenario)
    }
}