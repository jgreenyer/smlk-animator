package org.scenriotools.smlk.animator.model

open class InstanceLabelEvent (val instance : Instance, val label : String) : VisualEvent() {
}