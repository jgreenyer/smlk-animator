package org.scenriotools.smlk.animator.model

import javafx.beans.binding.StringBinding
import javafx.beans.property.SimpleDoubleProperty
import javafx.collections.ObservableList
import javafx.scene.paint.Color
import org.scenariotools.smlk.event
import tornadofx.*

open class Instance (val name : String, var xPos : Double, var yPos : Double, var width : Double, var height : Double, var color : Color = c("#000000"), var bgcolor : Color = c("#ffffff")) {

    val dependencies : ObservableList<Instance> = listOf<Instance>().toObservable()

    val shadowSizeProperty = SimpleDoubleProperty(0.0)
    var shadowSize by shadowSizeProperty

    fun setShadowSize(value : Double) = event(value){
        this.shadowSize = value
    }

    open fun getPropertyList() : List<Pair<String, StringBinding>> {
        return emptyList()
    }


}