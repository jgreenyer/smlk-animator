package org.scenriotools.smlk.animator.model

open class InstanceEvent (val instance : Instance, val name : String, vararg val parameters : String) : VisualEvent(){
}