package org.scenriotools.smlk.animator.model

import javafx.beans.property.*
import javafx.collections.ObservableList
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.smlkextension.LabelHelper
import tornadofx.*
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

open class InteractionSystem (var name : String, width : Double, height : Double,
                              val instances: ObservableList<Instance> = listOf<Instance>().toObservable(),
                              val interactionSystemConfiguration: MutableMap<String,InteractionSystemConfiguration> = mutableMapOf<String,InteractionSystemConfiguration>(),
                              var scenarioProgram : ScenarioProgram?,
                              var parameterToCharSequence: (Any) -> CharSequence = {obj -> obj.toString()}
) {

    var interactionSystemConfigurations : InteractionSystemConfigurations = InteractionSystemConfigurations(mapOf())

    private var isScenarioProgramRunning = false
    val canMakeNextStepProperty = SimpleBooleanProperty(true)
    var canMakeNextStep by canMakeNextStepProperty

    val scenarioProgramIsRunningProperty = SimpleBooleanProperty(false)
    var scenarioProgramIsRunning by scenarioProgramIsRunningProperty

    val currentEvents = listOf<VisualEvent>().toObservable()
    val recordedEvents = listOf<VisualEvent>().toObservable()

    var eventsNotToVisualize: IEventSet = NOEVENTS

    val configurationNames = interactionSystemConfigurations.configurations.keys.toTypedArray().toList().toObservable()

    val selectedConfigurationNameProperty = SimpleStringProperty("")
    var selectedConfigurationName by selectedConfigurationNameProperty

    val widthProperty = SimpleDoubleProperty()
    var width by widthProperty

    val heightProperty = SimpleDoubleProperty()
    var height by heightProperty

    init {
        this.height = height
        this.width = width
    }

    fun play() : String{
        var returnMessage = ""
        scenarioProgram?.let{
            runBlocking {
                while(canMakeNextStep){
                    doMakeStep(it, 3000)
                }
            }
        }
        return returnMessage
    }

    fun makeStep() : String{

        if (scenarioProgram == null){
            val interactionSystemConfiguration = interactionSystemConfigurations.configurations[selectedConfigurationName]
            interactionSystemConfiguration?.let {
                name = selectedConfigurationName
                width = interactionSystemConfiguration.width
                height = interactionSystemConfiguration.height
                instances.clear()
                instances.addAll(interactionSystemConfiguration.instances.toObservable())
                eventsNotToVisualize = it.eventsNotToVisualize
                scenarioProgram = interactionSystemConfiguration.createScenarioProgram()
                scenarioProgramIsRunning = true
            }
      }

        scenarioProgram?.let {sp ->
            if (!isScenarioProgramRunning) {
                sp.init()
                println("selectedConfiguration:" + selectedConfigurationName)
                canMakeNextStep = sp.canMakeNextStep()
                isScenarioProgramRunning = sp.canMakeNextStep()
            }

            currentEvents.clear()

            var stepEvent : Event? = null
            if(sp.canMakeNextStep()){
                runBlocking { stepEvent = doMakeStep(sp) }
            }

            if(!sp.canMakeNextStep()){
                canMakeNextStep = false
                if (!sp.guaranteeScenariosInMustProgressSection().isEmpty()) {
                    return "Guarantee scenarios in must-progress section: ${sp.guaranteeScenariosInMustProgressSection()}"
                }
                if (!sp.guaranteeViolationExceptions.isEmpty()) {
                    return sp.guaranteeViolationExceptions[0].message
                }
            }
        }
        return ""
    }

    private suspend fun doMakeStep(sp: ScenarioProgram, delayTime: Long = 0) : Event? {
        var eventVisible = false
        do{
            val event = sp.makeStep()
            if(event is ObjectEvent<*,*> && event.receiver is LabelHelper){ // label event
                val labelEvent = when {
                    event.type.kFunctionEquals(LabelHelper::label)
                            || event.type.kFunctionEquals(LabelHelper::envLabel)-> {
                        LabelEvent(event.parameters[0] as String)
                    }
                    else -> { // LabelHelper::instanceLabel
                        InstanceLabelEvent(event.parameters[0] as Instance, event.parameters[1] as String)
                    }
                }
                currentEvents.add(labelEvent)
                recordedEvents.add(labelEvent)
                eventVisible = true
                delay(delayTime)
                return event
            }
            else if (event is ObjectEvent<*,*> &&
                !eventsNotToVisualize.contains(event)
                && event.receiver is Instance){ // interaction event
                val interactionEvent = createInstanceEvent(event)
                currentEvents.add(interactionEvent)
                recordedEvents.add(interactionEvent)
                eventVisible = true
                delay(delayTime)
                return event
            }
        } while (!eventVisible && sp.canMakeNextStep())
        if (!sp.canMakeNextStep()) {
            canMakeNextStep = false
        }
        return null
    }


    private fun createInstanceEvent(objectEvent: ObjectEvent<*,*>): InstanceEvent {
        val receiver = objectEvent.receiver as Instance
        val paramtersSize = objectEvent.parameters.size
        return if(objectEvent.sender != null
            && instances.contains(objectEvent.sender)
            && instances.contains(receiver)){
            val sender = objectEvent.sender as Instance
            InteractionEvent(sender, receiver, objectEventString(objectEvent))
        }else {
            InstanceEvent(receiver, objectEventString(objectEvent))
        }
    }

    private fun objectEventString(objectEvent:ObjectEvent<*,*>) =
        "${objectEvent.type.name}${objectEvent.parameterString()}"

    fun exportSequenceDiagram() {
        scenarioProgram?.let {

            val currentTime = LocalDateTime.now()

            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss")
            val formattedTime = currentTime.format(formatter)
            val fileName = "src/main/resources/exportedSequenceDiagrams/${it.name}_${formattedTime}.puml"

            var fileContent = """@startuml
                |
            """.trimMargin()

            fileContent += generateLifelines()
            fileContent += generateEvents()

            fileContent += """
                |
|               @enduml
            """.trimMargin()

            File(fileName).writeText(fileContent)
        }
    }

    private fun generateLifelines() : String {
        var lifelineContent = ""
        for (instance in instances){
            lifelineContent += """
                
                participant "${instance.name}" as ${instance.name.removeWhitespaces()} #${instance.bgcolor.toString().substring(2,instance.bgcolor.toString().length-2)}
            """.trimIndent()
        }
        return lifelineContent
    }

    private fun generateEvents() : String {
        var eventsContent = ""
        for (visualEvent in recordedEvents){
            when (visualEvent) {
                is InteractionEvent -> {
                    val eventName = visualEvent.name.replace("\n", "\\n")
                    var arrowString = "->"
                    if (eventName.contains("Response|response".toRegex()))
                        arrowString = "-->"
                    eventsContent += """
                            
                            ${visualEvent.sourceInstance.name.removeWhitespaces()} ${arrowString} ${visualEvent.instance.name.removeWhitespaces()} : ${eventName}
                        """.trimIndent()
                }
                is InstanceEvent -> {
                    val eventName = visualEvent.name.replace("\n", "\\n")
                    val arrowString = "->"
                    eventsContent += """
                            
                            ${visualEvent.instance.name.removeWhitespaces()} ${arrowString} ${visualEvent.instance.name.removeWhitespaces()} : ${eventName}
                        """.trimIndent()
                }
                is InstanceLabelEvent -> {
                    val instance = visualEvent.instance
                    eventsContent += """
                        
                        hnote over ${instance.name.removeWhitespaces()} : ${visualEvent.label.replace("\n", "\\n")}
                    """.trimIndent()
                }
                is LabelEvent -> {
                    eventsContent += """
                
                        == ${visualEvent.label.replace("\n", "\\n")} ==
                    """.trimIndent()
                }
            }

        }
        return eventsContent
    }

    private fun String.removeWhitespaces() = this.replace("\\s".toRegex(), "")

    private fun firstAndLastInstanceString() =
        "${instances.first().name.removeWhitespaces()}, ${instances.last().name.removeWhitespaces()}"

    fun updateInteractionSystemConfigurations(interactionSystemConfigurations: InteractionSystemConfigurations) {
        this.interactionSystemConfigurations = interactionSystemConfigurations
        this.configurationNames.clear()
        this.configurationNames.addAll(interactionSystemConfigurations.configurations.keys)
    }


}

