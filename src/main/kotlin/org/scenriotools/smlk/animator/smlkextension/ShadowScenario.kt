package org.scenriotools.smlk.animator.smlkextension

import org.scenariotools.smlk.ALLEVENTS
import org.scenariotools.smlk.ObjectEvent
import org.scenariotools.smlk.event
import org.scenariotools.smlk.scenario
import org.scenriotools.smlk.animator.model.Instance

object ShadowHelper{
    fun setShadowSize(instance : Instance, shadowSize : Double) = event(instance, shadowSize){
        instance.shadowSize = shadowSize
    }
}

val shadowScenario = scenario(){
    while(true){
        val e = waitFor(ALLEVENTS)
        if (e is ObjectEvent<*,*> && e.receiver is Instance){
            val instance = e.receiver as Instance
            commit(ShadowHelper.setShadowSize(instance, instance.shadowSize+2.0))
        }
    }
}