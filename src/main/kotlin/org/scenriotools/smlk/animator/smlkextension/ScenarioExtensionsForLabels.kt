package org.scenriotools.smlk.animator.smlkextension

import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.model.Instance

object LabelHelper{
    fun instanceLabel(instance : Instance, label : String) = event(instance, label){}
    fun envInstanceLabel(instance : Instance, label : String) = event(instance, label){}
    fun label(label : String) = event(label){}
    fun envLabel(label : String) = event(label){}
}

suspend fun Scenario.instancelabel(instance : Instance, label : String) = commit(ConcreteEventSet(LabelHelper.instanceLabel(instance, label), LabelHelper.envInstanceLabel(instance, label)))
suspend fun Scenario.label(label : String) = commit(ConcreteEventSet(LabelHelper.label(label),  LabelHelper.envLabel(label)))

fun ObjectEvent<*,*>.isLabelEvent() =  this.receiver is LabelHelper
fun ObjectEvent<*,*>.instanceLabel() : Pair <Instance, String> = this.receiver as Instance to this.parameters[1] as String