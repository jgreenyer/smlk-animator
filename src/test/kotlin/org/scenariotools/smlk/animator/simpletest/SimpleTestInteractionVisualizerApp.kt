package org.scenariotools.smlk.animator.simpletest

import javafx.stage.Stage
import org.scenariotools.smlk.*
import org.scenriotools.smlk.animator.model.InteractionSystemConfiguration
import org.scenriotools.smlk.animator.model.InteractionSystemConfigurations
import org.scenriotools.smlk.animator.smlkextension.label
import org.scenriotools.smlk.animator.view.InteractionAnimatorView
import org.scenriotools.smlk.animator.style.InteractionAnimatorStyles
import tornadofx.App
import tornadofx.find
import tornadofx.launch

fun main(args: Array<String>) {
    launch<SimpleTestInteractionVisualizerApp>(*args)
}


class SimpleTestInteractionVisualizerApp: App(InteractionAnimatorView::class, InteractionAnimatorStyles::class){

    override fun start(stage: Stage) {

       super.start(stage)

        val view = find(primaryView, scope)

        val interactionAnimatorView = view as InteractionAnimatorView
        val interactionSystem = interactionAnimatorView.isvm.item

        val environmentMessageTypes =  listOf(PongMe::start)
        val instances = listOf(Env, PingMe, PongMe)
        val guaranteeScenarios = setOf(

            scenario(Env sends PongMe.start()) {
                println("Ping requesting ping(1) ** starting :)")
                request(PongMe sends PingMe.ping(1))

                interruptingEvents.add(ANY sends PongMe receives PongMe::pong param listOf(101..200))

                while (true){
                    val lastPongMessageEvent = waitFor(PingMe sends PongMe receives PongMe::pong param ("y" to 1..100))
                    val i = ((lastPongMessageEvent.parameters[0] as Int) + (lastPongMessageEvent.parameters[0]) as Int)
                    request(PongMe sends PingMe.ping(i))
                }
            },

            scenario(PongMe sends PingMe receives PingMe::ping param ("x" to 1..200)){
                val i = (it.parameters[0] as Int)
                println("Pong requesting PingMe sends pong($i, ${i+1})")
                request(PingMe sends PongMe.pong(i, i+1))
                println("Pong terminating")
            }
        )

        val assumptionScenariosOneStart =

            scenario {
                label("Init")
                request(Env sends PongMe.start())
            }



        val assumptionScenariosTwoStarts =

            scenario {
                label("Init")
                request(Env sends PongMe.start())
                request(Env sends PongMe.start())
            }



        interactionSystem.updateInteractionSystemConfigurations (
            InteractionSystemConfigurations(
                mapOf(
                    "Simple Interaction System One Start" to InteractionSystemConfiguration(
                            800.0, 800.0,
                        environmentMessageTypes = environmentMessageTypes,
                        instances = instances,
                        testScenario = assumptionScenariosOneStart,
                        guaranteeScenarios = guaranteeScenarios
                    ),
                    "Simple Interaction System Two Starts" to InteractionSystemConfiguration(
                            500.0, 500.0,
                        environmentMessageTypes = environmentMessageTypes,
                        instances = instances,
                        testScenario = assumptionScenariosTwoStarts,
                        guaranteeScenarios = guaranteeScenarios
                    )
                )
            )
        )
        interactionAnimatorView.title = "Interaction Animator: Simple Example Interaction"

    }

}

