package org.scenariotools.smlk.animator.simpletest

import javafx.beans.binding.StringBinding
import javafx.beans.property.Property
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import org.scenariotools.smlk.event
import org.scenriotools.smlk.animator.model.Instance
import tornadofx.*
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

object Env : Instance("Env", 100.0, 100.0, 80.0, 60.0, color = c("#4fc3f7"), bgcolor = c("#314147")) {
}

object PingMe : Instance("PingMe", 320.0, 200.0, 80.0, 60.0) {
    val lastXProperty = SimpleIntegerProperty(0)
    var lastX by lastXProperty

    fun ping(x : Int) = event(x){ lastX = x}

    override fun getPropertyList(): List<Pair<String, StringBinding>> {
        return  listOf("lastX" to lastXProperty.asString())
    }
}

object PongMe : Instance("PongMe", 120.0, 350.0, 80.0, 60.0)  {
    val lastXProperty = SimpleIntegerProperty(0)
    var lastX by lastXProperty
    val lastYProperty = SimpleIntegerProperty(0)
    var lastY by lastYProperty

    fun start() = event(){}
    fun pong(x : Int, y : Int) = event(x, y){ lastX = x; lastY = y}

    override fun getPropertyList(): List<Pair<String, StringBinding>> {
        return  listOf(
            "lastX" to lastXProperty.asString(),
            "lastY" to lastYProperty.asString()
        )
    }
}